<?php 
include_once "includes/connect.php" ;
$stmt = new DBC;
?>

<?php include "includes/header.php" ?>


<?php include "includes/funcs.php" ?>
    <!-- Navigation -->
    <?php include "includes/navigation.php" ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

               

                <!-- First Blog Post -->

                <?php 

                if(isset($_GET['cat'])){
                
                
                // $query  = $stmt->connect()->query("SELECT * FROM posts WHERE postTags LIKE '%".$_POST['search']."%' OR postTitle LIKE '%".$_POST['search']."%'");
                $query  = $stmt->connect()->prepare("SELECT * FROM posts WHERE postCategoryID=:pID AND postStatus='published' ORDER BY postID DESC");
                $query->bindValue(":pID",$_GET['cat'],PDO::PARAM_INT);

                $query->execute();

                $data = $query->fetchAll(PDO::FETCH_ASSOC);

                if(empty($data)){
                   echo "<h1 class='page-header'> "."Not Found"."</h1>";

                }
                
                foreach ($data as $row):
                
                ?>

                <h2>
                    <a href="post.php?pID=<?php echo $row['postID']; ?>"><?php echo $row['postTitle']; ?></a>
                </h2>
                <p class="lead">
                by <a href="authorPosts.php?uPost=<?php echo $row['postAuthor']; ?>&val=search"><?php echo $row['postAuthor']; ?></a>
                </p>
                <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $row['postDate']; ?> <span class="glyphicon glyphicon-comment" style="margin-left:10px;"></span>
                <?php 
                    
                    echo (returnCountComment($row['postID'],NULL));
                ?>
                Comment</p>
                <hr>
                <?php if(isset($row['postImage']) AND $row['postImage'] != ''){ ?>
                <img class="img-responsive" src="./images/<?php echo $row['postImage']; ?>" alt="">
                <hr>
                <?php } ?>
                <p>

                <?php echo substr($row['postContent'],0,350)."..."; ?>
                </p>
                <a class="btn btn-primary" href="post.php?pID=<?php echo $row['postID']; ?>">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

                <hr>

                <?php endforeach ;
                
                         }
                ?>

                <!-- Pager -->
                <ul class="pager">
                    <li class="previous">
                        <a href="#">&larr; Older</a>
                    </li>
                    <li class="next">
                        <a href="#">Newer &rarr;</a>
                    </li>
                </ul>

            </div>

            <!-- Blog Sidebar Widgets Column -->
          <?php include "includes/sidebar.php" ?>

        </div>
        <!-- /.row -->

        <hr>

    <?php include "includes/footer.php" ?>