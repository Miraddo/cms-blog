# CMS Blog

Simple Blog System

I made this system by a course on [udemy](https://www.udemy.com/php-for-complete-beginners-includes-msql-object-oriented/) but I did all part in different way, be honest I didn't watch all part of the course and if you pay attention in this system I used **PDO** instead of **mysqli** and unfortunately the template is same. I should using new version of SB Admin that made by bootstrap4 instead of bootstrap3.

## Getting Started

To running this CMS on your system you need something like [XAMPP](https://www.apachefriends.org/) or [MAMP](https://www.mamp.info/en). If you are a GNU/Linux user you can install LAMP(Linux, Apache, MySQL, PHP) or LEMP(Linux, Nginx, MariaDB, PHP) on your system. 

If you search in Google you can find a lot of good articles about how to install them.

### Installing

What things you need to install the CMS? Actually you don't need anything, Just you should config mysql connection and import tables in your database, To do that first go to _sqlfolder_ than upload or run [___cmsQuery___](sqlfolder/cmsQuery.sql) in your mysql or mariadb database. than put your database info in includes/connect.php

```
// open includes/connect.php

$this->host = "localhost"; // put your address here 
$this->username = "root";  // here you can set username
$this->password = "";      // if you set password on your database you should set it here 
$this->db = "cmsDB";       // put name of database that you created
```
After import ___cmsQuery___ there is an admin user you can login with it to CMS.

Just enter **admin** as the username and **admin1** as the password


## Built With

- [SB Admin Page](https://blackrockdigital.github.io/startbootstrap-sb-admin/) - The template used (Not new version I just used the old version that made by bootstrap3)
- [Google Chart](https://developers.google.com/chart/) - Used to show some information as a chart
- [CKEditor 5](https://ckeditor.com/ckeditor-5/) - Used to write better content

## Authors

- **Milad Poshtdari** - *Initial work* - [Miraddo](https://gitlab.com/Miraddo)

See also the list of [contributors](https://gitlab.com/Miraddo/cms-blog/-/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

