<?php 
include_once "includes/connect.php" ;
$stmt = new DBC;
?>

<?php include "includes/header.php" ?>


<?php include "includes/funcs.php" ?>
    <!-- Navigation -->
    <?php include "includes/navigation.php" ?>

    <!-- Page Content -->
    <div class="container">

        

    <?php 
                                

                                function updatedUser($pVal){
                                    global $stmt;
                               
                                    if($pVal === 1){
                                        $query= $stmt->connect()->prepare("UPDATE users SET  
                                    
                                            username=:uname,
                                            userFirstname=:fname,
                                            userLastname=:lname,  
                                            userEmail=:uemail,
                                            userPassword=:upass
                                            WHERE userID=:ID");
                                    
                                    $query->bindValue(":upass",password_hash($_POST['password'],PASSWORD_BCRYPT), PDO::PARAM_STR);
                                
                                    }else if($pVal === 0){
                                            $query= $stmt->connect()->prepare("UPDATE users SET  
                                    
                                            username=:uname,
                                            userFirstname=:fname,
                                            userLastname=:lname,  
                                            userEmail=:uemail
                                            WHERE userID=:ID");
                                    }
                                
                                    $query->bindValue(":uname", $_POST['username'], PDO::PARAM_STR);
                                    $query->bindValue(":ID", $_SESSION['userID'], PDO::PARAM_INT);
                                    $query->bindValue(":fname", $_POST['userFirstname'], PDO::PARAM_STR);
                                    $query->bindValue(":lname", $_POST['userLastname'], PDO::PARAM_STR);
                                    $query->bindValue(":uemail", $_POST['userEmail'], PDO::PARAM_STR);
                                    $query->execute();
                                    
                                    $_SESSION['fname']=$_POST['userFirstname'];
                                    $_SESSION['lname']=$_POST['userLastname'];

                                    echo("
                   
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <strong>Profile updated!</strong> Your post updated successfully.                    </div> 
            ");
                                }
                                
                                
                                
                                if(isset($_POST['updateUser'])){

                                    if(strlen($_POST['password']) < 4){
                                        updatedUser(0);
                                    }else if(strlen($_POST['password']) > 4){
                                        updatedUser(1);
                                    }
                                }
                                
                                
                                
                                
                                
                                
                                                            
                                ?>
                                <!-- Page Heading -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h1 class="page-header">
                                            Edit Profile
                                            <!-- <small>Author</small> -->
                                        </h1>
                                
                                    </div>
                                
                                
                                        <form action="" method="post" enctype="multipart/form-data">
                                        <?php
                                            $vUserQuery = $stmt->connect()->prepare("SELECT * FROM users WHERE userID=?");
                                            $vUserQuery->bindValue(1,$_SESSION['userID'], PDO::PARAM_INT );
                                            $vUserQuery->execute();
                                            $vUserData = $vUserQuery->fetchAll(PDO::FETCH_ASSOC); 
                                            foreach($vUserData as $row):
                                        ?>
                                            <div class="form-group">
                                                <label for="username"> Username </label>
                                                <input type="text" name="username" class="form-control" value="<?php echo $row['username'];?>">
                                            </div>
                                
                                           
                                
                                            <div class="form-group">
                                                <label for="userFirstname">Firstname</label>
                                                <input type="text" name="userFirstname" class="form-control" value="<?php echo $row['userFirstname'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="userLastname">Lastname</label>
                                                <input type="text" name="userLastname" class="form-control" value="<?php echo $row['userLastname'];?>">
                                            </div>
                                            <!-- <div class="form-group">
                                                <label for="userUpload">Upload</label>
                                                <?php if(isset($row['userImage']) AND $row['userImage'] != ''){ ?>
                                                        
                                                    <img class="img-responsive" src="../images/users/<?php echo $row['userImage'];?>" width="64px"> 
                                                    <br>
                                                <?php }else{
                                                    echo " Image Not Found ";
                                                } ?>
                                                <input type="file" name="userUpload" value="<?php echo $row['userImage'];?>">
                                            </div> -->
                                            <div class="form-group">
                                                <label for="userEmail">Email</label>
                                                <input type="email" name="userEmail" class="form-control" value="<?php echo $row['userEmail'];?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input type="password" name="password" class="form-control" >
                                            </div>
                                
                                            
                                            <?php endforeach; ?>
                                
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-sm btn-success" name="updateUser" value="Update Profile">
                                            </div>
                                
                                        </form>
                                
                                
                                
                                
                                </div>
                                
                                
                                
                                
                                <!-- /.row -->
        <!-- /.row -->

        <hr>

    <?php include "includes/footer.php" ?>