<?php 
include_once "includes/connect.php" ;
$stmt = new DBC;
?>

<?php include "includes/header.php" ?>


<?php include "includes/funcs.php" ?>
    <!-- Navigation -->
    <?php include "includes/navigation.php" ?>




    <?php



        $session = session_id();
        $time = time();
        $timeOutInSeconds=300;
        $timeOut=$time - $timeOutInSeconds;

        $queryStatus = $stmt->connect()->prepare("SELECT * FROM usersStatus WHERE `session` = ?");
        $queryStatus->bindValue(1,$session,PDO::PARAM_STR);
        $queryStatus->execute();
        $statusData=$queryStatus->rowCount();

        if ($statusData==NULL){
            $insertQueryStatus= $stmt->connect()->prepare("INSERT INTO usersStatus(`session`,`time`) VALUES (?,?)");
            $insertQueryStatus->bindValue(1,$session,PDO::PARAM_STR);
            $insertQueryStatus->bindValue(2,$time,PDO::PARAM_INT);
            $insertQueryStatus->execute();
        }else{
            $updateQueryStatus= $stmt->connect()->prepare("UPDATE usersStatus SET `time`=? WHERE `session`=?");
            $updateQueryStatus->bindValue(1,$time,PDO::PARAM_INT);
            $updateQueryStatus->bindValue(2,$session,PDO::PARAM_STR);
            $updateQueryStatus->execute();
        }
    
    
    ?>
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

                <!-- <h1 class="page-header">
                    Page Heading
                    <small>Secondary Text</small>
                </h1> -->

                <!-- First Blog Post -->

                <?php 
                
                if(isset($_GET['page'])){

                    $res = preg_replace("/[^0-9]/", "", $_GET['page']);
                    $page =intval($res);

                }else{
                    $page="";
                }

                if($page=="" || $page==1){
                    $valPage=0;
                }else{
                    $valPage=($page * 5)-5;

                }

                $countPosts = ceil (returnCountPost()/5);


                $query  = $stmt->connect()->query("SELECT * FROM posts WHERE postStatus='published' ORDER BY postID DESC LIMIT $valPage,5");

                $data = $query->fetchAll(PDO::FETCH_ASSOC);
                if(empty($data)){
                    echo "<h1>Not Found</h1><hr>";
                }
                foreach ($data as $row):
                
                ?>

                <h2>
                    <a href="post.php?pID=<?php echo $row['postID']; ?>"><?php echo $row['postTitle']; ?></a>
                </h2>
                <p class="lead">
                by <a href="authorPosts.php?uPost=<?php echo $row['postAuthor']; ?>&val=search"><?php echo $row['postAuthor']; ?></a>
                </p>
                <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $row['postDate']; ?> <span class="glyphicon glyphicon-comment" style="margin-left:10px;"></span>
                <?php 
                    
                    echo (returnCountComment($row['postID'],NULL));
                ?>
                Comment</p>
                <hr>
                <?php if(isset($row['postImage']) AND $row['postImage'] != ''){ ?>
                <img class="img-responsive" src="./images/<?php echo $row['postImage']; ?>" alt="">
                <hr>
                <?php } ?>
                
                <p>

                <?php echo html_entity_decode(substr($row['postContent'],0,350)."...");  ?>
                </p>
                <a class="btn btn-primary" href="post.php?pID=<?php echo $row['postID']; ?>">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

                <hr>

                <?php endforeach ?>

                <!-- Pager -->
                <ul class="pager">
                    <!-- <li class="previous">
                        <a href="#">&larr; Older</a>
                    </li>
                    <li class="next">
                        <a href="#">Newer &rarr;</a>
                    </li> -->
                
                    <?php

                    if(!empty($data)){

                        if (isset($_GET['page'])) {
                            $num = intval(preg_replace("/[^0-9]/", "", $_GET['page'])); 
                        }else{
                            $num = 1;
                        }
                        for ($i=1; $i <= $countPosts; $i++){

                            if($i === $num){
                                echo "<li><a class='activeLink' href='index.php?page={$i}'>{$i}</a></li>";    
                            }else{
                                echo "<li><a href='index.php?page={$i}'>{$i}</a></li>";    

                            }
                        }
                    }
                    
                    ?>
                </ul>

            </div>

            <!-- Blog Sidebar Widgets Column -->
          <?php include "includes/sidebar.php" ?>

        </div>
        <!-- /.row -->

        <hr>

    <?php include "includes/footer.php" ?>