<div class="col-md-4">
<?php
    
    if(isset($_SESSION['loginPart'])){
    if ($_SESSION['loginPart']==='0') {
        ?>


        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Warning!</strong> There is a problem! If you're not register please register <a href="register.php">here </a>
        </div>
        

        <?php
        $_SESSION['loginPart']='1';
    }
}
    ?>
<!-- Blog Search Well -->
<div class="well">
    <h4>Blog Search</h4>
    <form action="search.php" method="post">

    <div class="input-group">
            <input name="search" type="text" class="form-control" placeholder="Search">
                <span class="input-group-btn">
                    <button name="submit" class="btn btn-default" type="submit">
                        <span class="glyphicon glyphicon-search"></span>
                </button>
            </span>
    
    </div>
    </form>
    <!-- /.input-group -->
</div>

<!-- Blog Categories Well -->
<div class="well">
    <h4>Blog Categories</h4>
   <div class="row">
        <div class="col-lg-12">
            <ul class="list-unstyled">
               
            <?php 
                        
                        $query = $stmt->connect()->query("SELECT * FROM `categories` ");
                        $data=$query->fetchAll(PDO::FETCH_ASSOC);
                        foreach($data as $row):

                            echo ("<li><a href='category.php?cat={$row['catID']}''> ".$row['catTitle']."</a></li>");

                        endforeach;

                   
                   
                   ?>
            </ul>
        </div>
        </div>
  
    <!-- /.row -->
</div>
<?php if(isset($_SESSION['uRole'])){ ?>

<div class="well">
    <h4 class="text-info ">Welcome, <span class="text-capitalize"> <?php echo $_SESSION['fname'].' '.$_SESSION['lname']; ?></span></h4>

    <a href="includes/logout.php" class="btn btn-sm btn-danger">Logout</a>
</div>
<?php 

}else{ ?>

<div class="well">
    <h4>Login</h4>
   <form action="includes/login.php" method="post">
        <div class="form-group">
            <input type="text" class="form-control" name="username" placeholder="Username">
        </div>
        <div class="input-group">
            <input type="password" class="form-control" name="password" placeholder="Password">
            <span class="input-group-btn">
                <button class="btn btn-primary" name="submitLogin" type="submit">Submit</button>
            </span>
        </div>
   </form>
</div>
<?php } ?>

<div class="well">
    <h4>Blog Posts</h4>

        <!-- /.col-lg-6 -->
        <div class="row">
        <div class="col-lg-12">
            <ul class="list-unstyled">
            <?php    
                        $query = $stmt->connect()->query("SELECT postID,postTitle,postStatus FROM `posts` WHERE postStatus='published'  ORDER BY postID DESC LIMIT 0,10");
                        $data=$query->fetchAll(PDO::FETCH_ASSOC);
                        foreach($data as $row){

                            echo ("<li><a href='post.php?pID=".$row['postID']."'>".$row['postTitle']."</a></li>");

                        }
                   
                   
                   ?>
            </ul>
        </div>
        <!-- /.col-lg-6 -->
        </div>
</div>
<!-- Side Widget Well -->
<?php include "widget.php"; ?>

</div>