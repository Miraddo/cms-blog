<?php 


function returnCountComment($kID){
    global $stmt;
    $queryCommentNumber  = $stmt->connect()->prepare("SELECT IFNULL(COUNT(*),0) as returnVal FROM comments WHERE commentPostID=? AND commentStatus='approved'");
    
    $queryCommentNumber->bindValue(1,$kID,PDO::PARAM_INT);
    $queryCommentNumber->execute();
    $CNData= $queryCommentNumber->fetch(PDO::FETCH_ASSOC);
    return $CNData['returnVal'];
}


function returnCountPost(){
    global $stmt;
    $queryPostNumber  = $stmt->connect()->prepare("SELECT IFNULL(COUNT(*),0) as returnVal FROM posts WHERE postStatus='published'");
    $queryPostNumber->execute();
    $data= $queryPostNumber->fetch(PDO::FETCH_ASSOC);
    return $data['returnVal'];
}



?>