 <!-- Blog Comments -->
<?php include_once "includes/funcs.php" ?>

 <?php

$check = 0;

function submitComment($id,$name,$email,$content){
    global $stmt,$postTitle;
    $submitCommentQuery= $stmt->connect()->prepare("INSERT INTO comments 
    (commentDate,commentPostID,commentAuthor,commentPostTitle,commentEmail,commentContent) 
    VALUES 
    (NOW(),:pID,:author,:postTitle,:email,:content)");
    $submitCommentQuery->bindValue(":email", $email , PDO::PARAM_STR);
    $submitCommentQuery->bindValue(":pID", $id, PDO::PARAM_INT);
    $submitCommentQuery->bindValue(":author", $name, PDO::PARAM_STR);
    $submitCommentQuery->bindValue(":postTitle", $postTitle , PDO::PARAM_STR);
    $submitCommentQuery->bindValue(":content", $content, PDO::PARAM_STR);
    $submitCommentQuery->execute();

    echo "<h2 class='text-success'>Comment Sent!</h2>";

}
if (isset($_POST['commentSubmit'])){
if((!empty($_POST['name'])) && (!empty($_POST['email']))){
    if(!empty($_POST['content'])){
        if($check === 1){
            echo "<h2 class='text-danger'>Your commnet is not approved yet! by Admin</h2>";
        }else{
            submitComment($_GET['pID'],$_POST['name'],$_POST['email'],$_POST['content']);
            $check = 1;
    
            $queryCommentNumber  = $stmt->connect()->prepare("SELECT IFNULL(COUNT(*),0) as returnVal FROM comments WHERE commentPostID=?");    
            $queryCommentNumber->bindValue(1,$_GET['pID'],PDO::PARAM_INT);
            $queryCommentNumber->execute();
            $CNData= $queryCommentNumber->fetch(PDO::FETCH_ASSOC);
            $countPostComment = $stmt->connect()->prepare("UPDATE posts SET postCommentCount =:pcc WHERE postID=:pID");
            $countPostComment->bindValue(":pID", $_GET['pID'], PDO::PARAM_INT);
            $countPostComment->bindValue(":pcc", $CNData['returnVal'], PDO::PARAM_INT);
            $countPostComment->execute();
        }
    }else{
        ?>
        <div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Warning!</strong> Your content couldn't be empty!.
</div>
        <?php
    }
}else {
    ?>

<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Warning!</strong> Name or Email couldn't be empty !.
</div>

    <?php
}
}
?>
 <!-- Comments Form -->
 <div class="well">
     <h4>Leave a Comment:</h4>
     <form action="" method="post" role="form">
         <div class="form-inline" style="margin-bottom:10px;">
             <div class="form-group" style="margin-right:20px;">
                 <label for="exampleInputName2">Name</label>
                 <input type="text" class="form-control" id="exampleInputName2" name="name" value="<?php echo $_SESSION['fname']. ' ' . $_SESSION['lname']; ?>" placeholder="John Smith">
             </div>
             <div class="form-group">
                 <label for="exampleInputEmail2">Email</label>
                 <input type="email" class="form-control" id="exampleInputEmail2" name="email" value="<?php echo $_SESSION['uEmail'];  ?>"
                     placeholder="john.smith@example.com">
             </div>
         </div>
         <div class="form-group">
             <textarea class="form-control" name="content" rows="3"></textarea>
         </div>
         <button type="submit" name="commentSubmit" class="btn btn-primary">Submit</button>
     </form>
 </div>

 <hr>

 <!-- Posted Comments -->
<?php 

$returnCommentQuery= $stmt->connect()->prepare("SELECT * FROM comments WHERE commentPostID=:id AND commentStatus='approved'");
$returnCommentQuery->bindValue(":id",$_GET['pID'],PDO::PARAM_INT);
$returnCommentQuery->execute();

$returnCommentData = $returnCommentQuery->fetchAll(PDO::FETCH_ASSOC);
if (empty($returnCommentData)) {
    echo "<h2 class='text-info'>Be first one who comment here!</h2>";
}
foreach ($returnCommentData as $key) {
    
    extract($key);

    $email = $commentEmail;
    $default = "http://i0.wp.com/mindvision.ir/cms/images/users/user.png";
    $size = 64;
    $grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;

?>
 <!-- Comment -->
 <div class="media">
     <a class="pull-left" href="#">
         <img class="media-object" src="<?php echo $grav_url; ?>" alt="">
     </a>
     <div class="media-body">
         <h4 class="media-heading">
             <small><span class="text-capitalize" style="font-size:16px;font-weight:bolder; color:black;"><strong><?php echo $commentAuthor ?></strong></span> at <?php echo $commentDate ?></small>
         </h4>
         <?php echo $commentContent ?>
         <!-- Nested Comment -->
         <!-- <div class="media">
             <a class="pull-left" href="#">
                 <img class="media-object" src="http://placehold.it/64x64" alt="">
             </a>
             <div class="media-body">
                 <h4 class="media-heading">Nested Start Bootstrap
                     <small>August 25, 2014 at 9:30 PM</small>
                 </h4>
                 Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.
                 Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi
                 vulputate fringilla. Donec lacinia congue felis in faucibus.
             </div>
         </div> -->
         <!-- End Nested Comment -->
        
     </div>
</div>
<?php } ?>
