    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="index.php">CMS Blog</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                
                   <?php       

                        $query = $stmt->connect()->query("SELECT * FROM `categories` ");
                        $data=$query->fetchAll(PDO::FETCH_ASSOC);
                        foreach($data as $row){
                            echo ("<li><a href='category.php?cat={$row['catID']}'> ".$row['catTitle']."</a></li>");
                        }
                        
                        
                   ?>
                
            
                </ul>

                <ul class="nav navbar-nav navbar-right">
                        <?php
                        
                        if(isset($_SESSION['uRole']) && $_SESSION['uRole'] === "admin"){
                            if(isset($_GET['pID'])){
                                echo "
                                    <li><a href='./admin/posts.php?source=editPost&pID={$_GET['pID']}'> Edit Post</a></li>
                                
                                ";
                            }

                            echo "
                            <li><a href='./admin'> Admin</a></li>
                            <li><a href='includes/logout.php' >Logout</a></li>

                            ";
                        }



                        if(isset($_SESSION['uRole']) && $_SESSION['uRole'] === "subscriber"){
                            
                                echo "
                                    <li><a href='profile.php'> Profile</a></li>
                                    <li><a href='includes/logout.php' >Logout</a></li>

                                ";
                            
                        }


                        if(!isset($_SESSION['uRole'])){
                            
                            echo "
                                <li><a href='register.php'>Register</a></li>
                                <li><a href='login.php' >Login</a></li>

                            ";
                        
                    }
                     
                        ?>

        
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>