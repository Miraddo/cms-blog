<?php 
include_once "includes/connect.php" ;
$stmt = new DBC;
?>

<?php include "includes/header.php" ?>

<?php
if(!isset($_GET['pID'])){
    header("Location: /learning/cms/ ");
}
?>


    <!-- Navigation -->
    <?php include "includes/navigation.php" ?>
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="col-lg-8">

                <!-- Blog Post -->
                <?php 
   
$query  = $stmt->connect()->prepare("SELECT * FROM posts  WHERE (postID=:pID AND postStatus='published') OR (postID=:pID 
                                        AND 
                                    (SELECT userRole FROM users WHERE username=:uname)='admin')");
$query->bindValue(":pID", $_GET['pID'] , PDO::PARAM_INT);
$query->bindValue(":uname", $_SESSION['username'],PDO::PARAM_STR);
$query->execute();
$data = $query->fetchAll(PDO::FETCH_ASSOC);

if (empty($data)){
    header("Location: /learning/cms/ ");
}else{
$postViewed  = $stmt->connect()->prepare("UPDATE posts SET postViewsCount= postViewsCount+1 WHERE postID=:pID");
$postViewed->bindValue(":pID", $_GET['pID'],PDO::PARAM_INT);
$postViewed->execute();

}
foreach ($data as $row){
    




?>
                <!-- Title -->
                <h1><?php
                   $postTitle=$row['postTitle'];
                
                    echo $row['postTitle']; ?></h1>

                <!-- Author -->
                <p class="lead">
                by <a href="authorPosts.php?uPost=<?php echo $row['postAuthor']; ?>&val=search"><?php echo $row['postAuthor']; ?></a>
                </p>

                <hr>

                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> Posted on <?php echo $row['postDate']; ?></p>

                <hr>

                <!-- Preview Image -->
                <?php if(isset($row['postImage']) AND $row['postImage'] != ''){ ?>
                <img class="img-responsive" src="./images/<?php echo $row['postImage']; ?>" alt="">
                <hr>
                <?php } ?>

               

                <!-- Post Content -->
                <!-- <p class="lead"></p> -->
                <p>
                    <?php echo html_entity_decode($row['postContent']); ?>
                </p>

                    <?php } ?>
                <hr>

                <?php include "includes/comments.php" ?>

            </div>

   
            <!-- Blog Sidebar Widgets Column -->
            <?php include "includes/sidebar.php" ?>

</div>
<!-- /.row -->

<hr>

<?php include "includes/footer.php" ?>