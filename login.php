<?php 
include_once "includes/connect.php" ;
$stmt = new DBC;
?>

<?php include "includes/header.php" ?>


    <!-- Navigation -->
    <?php include "includes/navigation.php" ?>

    <!-- Page Content -->
    <div class="container" style="width:480px">

                                <!-- Page Heading -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h1 class="page-header text-center">
                                            Login Page
                                            <!-- <small>Author</small> -->
                                        </h1>
                                
                                    </div>
                                
                                
                                        <form action="includes/login.php" method="post" enctype="multipart/form-data">
                               
                                            <div class="form-group">
                                                <label for="username"> Username </label>
                                                <input type="text" name="username" class="form-control" value="">
                                            </div>
                                
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input type="password" name="password" class="form-control" >
                                            </div>
                                
                                        
                                
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-success btn-block" name="submitLogin" value="Login">
                                            </div>
                                
                                        </form>
                                
                                
                                
                                
                                </div>
                                
                                
                                
                                
                                <!-- /.row -->
        <!-- /.row -->

        <hr>

    <?php include "includes/footer.php" ?>