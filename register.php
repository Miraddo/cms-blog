<?php
include_once "includes/connect.php";
$stmt = new DBC;
?>

<?php include "includes/header.php" ?>


<!-- Navigation -->
<?php include "includes/navigation.php" ?>

<!-- Page Content -->
<div class="container" style="width:480px">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center">
                Register Page
                <!-- <small>Author</small> -->
            </h1>

        </div>
        <?php
        if ($_SESSION['userEx'] === "1") {
            ?>
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Warning!</strong> This user already exist!
            </div>

        <?php
        } elseif ($_SESSION['userEx'] === "0") {
            ?>

            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Successfully!</strong> This user successfully registered! Login <a href="login.php">here</a>
            </div>

        <?php

        } elseif ($_SESSION['userEx'] === "2") {
            ?>

            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Warning!</strong> username, email and password couldn't be empty
            </div>

        <?php

        }

        $_SESSION['userEx'] = 3;

        ?>

        <form action="includes/register.php" method="post" enctype="multipart/form-data">

            <div class="form-group">
                <label for="username"> Username </label>
                <input type="text" name="username" class="form-control" value="">
            </div>



            <div class="form-group">
                <label for="userFirstname">Firstname</label>
                <input type="text" name="userFirstname" class="form-control" value="">
            </div>
            <div class="form-group">
                <label for="userLastname">Lastname</label>
                <input type="text" name="userLastname" class="form-control" value="">
            </div>

            <div class="form-group">
                <label for="userEmail">Email</label>
                <input type="email" name="userEmail" class="form-control" value="">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control">
            </div>



            <div class="form-group">
                <input type="submit" class="btn btn-success" name="registerUser" value="Register">
            </div>

        </form>




    </div>




    <!-- /.row -->
    <!-- /.row -->

    <hr>

    <?php include "includes/footer.php" ?>