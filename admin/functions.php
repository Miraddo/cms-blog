<?php  
 
function returnEditCat(){
    global $stmt;
    global $editValue;
    if(isset($_GET['edit'])){

        $dis= "show";
        $editKey = $_GET['edit'];

        $_SESSION['editCategory'] =  $editKey;

        $showEditQuery = $stmt->connect()->prepare("SELECT * FROM categories WHERE catID=:key");
        $showEditQuery->bindValue(':key',$editKey,PDO::PARAM_INT);
        $showEditQuery->execute();
        $editData = $showEditQuery->fetchAll(PDO::FETCH_ASSOC);

        foreach($editData as $row){
            $editValue = $row['catTitle'];
        }

        

    }
}



function deleteCat(){
    global $stmt;
    if(isset($_GET['delete'])){

        $delQuery= $stmt->connect()->prepare("DELETE FROM categories WHERE catID=:del ");
        $delQuery->bindValue(':del',$_GET['delete'],PDO::PARAM_INT);
        $delQuery->execute();
    }
}




function saveCat(){
    global $stmt;
    if(isset($_POST['submit'])){

        if(!empty($_POST['catTitle'])){
            $query= $stmt->connect()->prepare("INSERT INTO categories ( catTitle) VALUE ( :category ) ");
            $query->bindValue(':category', $_POST['catTitle'], PDO::PARAM_STR);
            $data = $query->execute();

            // $count= $data->rowCount();
            // if($count>0){
            //     echo("data is duplicated");
            // }else{
            //     echo("data is saved");
                
            // }
            
        }
    }
}


function updateCat(){
    global $stmt;
    if(isset($_POST['editSubmit'])){
        $dis = "hidden";
        

        $eKey = $_SESSION['editCategory'];
    
        $eCat = $stmt->connect()->prepare("UPDATE categories SET catTitle=:val WHERE catID=:k");
        $eCat->bindValue(':val',$_POST['editCat'],PDO::PARAM_STR);
        $eCat->bindValue(':k',$eKey,PDO::PARAM_INT);
        $eCat->execute();

        echo ("Value Updated!");
        
    }
}



function returnCat(){
    global $stmt;

    $query = $stmt->connect()->query("SELECT * FROM categories");
    $data = $query->fetchAll(PDO::FETCH_ASSOC);

    foreach($data as $row){
        echo "<tr>
        <td>".$row["catID"]."</td>
        <td>".$row["catTitle"]."</td>
        <td><a href='categories.php?delete=".$row['catID']."'> Delete</a></td>
        <td><a href='categories.php?edit=".$row['catID']."'> Edit</a></td>
        </tr>";
    }

}
?>