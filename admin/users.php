<?php include "includes/header.php" ?>

<div id="wrapper">
    <!-- Navigation -->

    <?php include "includes/navigation.php" ?>

    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <?php include "includes/sidebar.php" ?>





    <div id="page-wrapper">

        <div class="container-fluid">

<?php 

if(isset($_GET['source'])){

    $source = $_GET['source'];

}else{
    $source ="";
}

switch($source){
    case 'viewUsers':
        include "includes/viewUsers.php";
        break;
    case 'newUser':
        include "includes/newUser.php";
        break;
    case 'editUser':
        include "includes/editUser.php";
        break;
    case 'profile':
        include "includes/profile.php";
        break;
    default :
        include "includes/viewUsers.php";
        break;
}

?>







</div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<?php include "includes/footer.php" ?>