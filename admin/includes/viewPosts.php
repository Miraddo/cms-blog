



<?php 


    function actionDelete($key){
        global $stmt;
        $query = $stmt->connect()->prepare("DELETE FROM posts WHERE postID = ?");
        $query->bindValue(1,$key,PDO::PARAM_INT);
        $query->execute();
    }
    
    function actionPublish($key){
        global $stmt;
        $query = $stmt->connect()->prepare("UPDATE posts SET postStatus='published'  WHERE postID = ?");
        $query->bindValue(1,$key,PDO::PARAM_INT);
        $query->execute();
    }

    function actionDraft($key){
        global $stmt;
        $query = $stmt->connect()->prepare("UPDATE posts SET postStatus='drafted'  WHERE postID = ?");
        $query->bindValue(1,$key,PDO::PARAM_INT);
        $query->execute();
    }

    function actionClone($key){
        global $stmt;
        
                                
        $vPQuery = $stmt->connect()->prepare("SELECT * FROM posts WHERE postID=?");
        $vPQuery->bindValue(1,$key,PDO::PARAM_INT);
        $vPQuery->execute();
        $vPData = $vPQuery->fetchAll(PDO::FETCH_ASSOC);

        foreach($vPData as $row){
            $title=$row['postTitle'];
            $status=$row['postStatus'];
            $content=$row['postContent'];
            $author=$row['postAuthor'];
            $postCatID=$row['postCategoryID'];
            $image=$row['postImage'];
            $tags=$row['postTags'];
            $date=$row['postDate'];
        }
    
        $query= $stmt->connect()->prepare("INSERT INTO posts   (postCategoryID, 
        postTitle, 
        postAuthor, 
        postDate,
        postImage, 
        postContent,
        postTags,
        postStatus,
        postViewsCount) 
        VALUES 
        (?,?,?,?,?,?,?,?,0)");
        $query->bindValue(1, $postCatID, PDO::PARAM_INT);
        $query->bindValue(2, $title, PDO::PARAM_STR);
        $query->bindValue(3, $author, PDO::PARAM_STR);
        $query->bindValue(4, $date, PDO::PARAM_STR);
        $query->bindValue(5, $image, PDO::PARAM_STR);
        $query->bindValue(6, $content, PDO::PARAM_STR);
        $query->bindValue(7, $tags, PDO::PARAM_STR);
        $query->bindValue(8, $status, PDO::PARAM_STR);
        $query->execute();
    }

    if (isset($_POST['checkBoxArray'])) {

        switch ($_POST['bulkOption']) {
            case 'publish':
                foreach ($_POST['checkBoxArray'] as $array => $value) {
                    actionPublish($value);
                }
                break;
            case 'draft':
                foreach ($_POST['checkBoxArray'] as $array => $value) {
                    actionDraft($value);
                }
                break;
            case 'delete':
                foreach ($_POST['checkBoxArray'] as $array => $value) {
                    actionDelete($value);
                }
                break;
            case 'clone':
                foreach ($_POST['checkBoxArray'] as $array => $value) {
                    actionClone($value);
                }
                break;
           
        }


    }



?>



<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            View All Posts
        </h1>

    </div>









    <div class="col-lg-12">
        <form action="" method="post">

        <div class="col-md-4" style="margin-bottom:20px;">
            <select class="form-control w-25" name="bulkOption">
                <option selected>Select Options</option>
                <option value="publish">Publish</option>
                <option value="draft">Draft</option>
                <option value="clone">Clone</option>
                <option value="delete">Delete</option>
            </select>
        </div>

        <div class="col-md-4" style="margin-bottom:20px;">
            <input type="submit" name="submit" class="btn btn-sm btn-success" value="Apply">
            <a  class="btn btn-sm btn-primary" href="posts.php?source=newPost"> New Post</a>
        </div>
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th><input type="checkbox" name="selectAllBoxes" id="allBoxes"></th>
                    <th>ID</th>
                    <th>Author</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th>Image</th>
                    <th>Tags</th>
                    <th>Comments</th>
                    <th>Date</th>
                    <th>Viewed</th>

                </tr>
            </thead>
            <tbody>

                            <?php 
                                
                                    $vPostQuery = $stmt->connect()->query("SELECT * FROM posts ORDER BY postID DESC");
                                    $vPostData = $vPostQuery->fetchAll(PDO::FETCH_ASSOC);

                                    foreach($vPostData as $row):
                                
                            ?>

                <tr>
                    <td><input type="checkbox" class="checkBoxes" name="checkBoxArray[]" value="<?php echo $row['postID'];?>"></td>

                    <td><?php echo $row['postID'];?></td>
                    <td><?php echo $row['postAuthor'];?></td>
                    <?php 
                        if($row['postStatus']==='drafted'){
                            echo "<td>{$row['postTitle']}</td>";

                        }else{
                            echo "<td><a href='../post.php?pID={$row['postID']}'>{$row['postTitle']}</a></td>";

                        }
                    ?>
                    <td><?php 
                    
                    $cPostQuery = $stmt->connect()->query("SELECT * FROM categories WHERE catID={$row['postCategoryID']}");
                    $cPostData = $cPostQuery->fetchAll(PDO::FETCH_ASSOC);

                    echo $cPostData[0]['catTitle']; 
                    
                    ?></td>
                    <td><?php echo $row['postStatus'];?></td>
                    
                        
                    <?php if(isset($row['postImage']) AND $row['postImage'] != ''){ ?>
                        <td>
                        <img class="img-responsive" src="../images/<?php echo $row['postImage'];?>" width="120px"> 
                        </td>              
                <?php }else{
                    echo "<td class='info text-danger'> Image Not Found </td> ";
                } ?>
                    
                
                
                
             
                    <td><?php echo $row['postTags'];?></td>
                    <td><?php echo $row['postCommentCount'];?></td>
                    <td><?php echo $row['postDate'];?></td>
                    <td><?php echo $row['postViewsCount']; ?></td>
                    <td> <a href="posts.php?source=editPost&pID=<?php echo $row['postID'];?>">Edit</a> </td>
                    <td> <a onClick="javascript: alert('deleted')" href="posts.php?deleteCode=<?php echo $row['postID'];?>">Delete</a> </td>

                </tr>



                <?php endforeach; ?>

            </tbody>
        </table>
        </form>
    </div>
</div>


<?php

if (isset($_GET['deleteCode'])){
    
    actionDelete($_GET['deleteCode']);

}



?>

<!-- /.row -->