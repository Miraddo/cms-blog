<?php 
                                

function addedUser($valImg){
    global $stmt;
    if ($valImg===1){
        $valImg='';
    }
       
    $query= $stmt->connect()->prepare("INSERT INTO `users` 
        (`username`, 
        `userPassword`, 
        `userFirstname`, 
        `userLastname`, 
        `userEmail`, 
        `userImage`, 
        `userRole`) 
        VALUES (?, ?, ?, ?, ?, ?, ?)");
    $query->bindValue(1, $_POST['username'], PDO::PARAM_STR);
    $query->bindValue(2, password_hash($_POST['password'],PASSWORD_BCRYPT), PDO::PARAM_STR);
    $query->bindValue(3, $_POST['userFirstname'], PDO::PARAM_STR);
    $query->bindValue(4, $_POST['userLastname'], PDO::PARAM_STR);
    $query->bindValue(5, $_POST['userEmail'], PDO::PARAM_STR);
    $query->bindValue(6, $valImg, PDO::PARAM_STR);
    $query->bindValue(7, $_POST['role'], PDO::PARAM_STR);
    $query->execute();
    echo("<h2>Added Successfuly</h2>");

}



if(isset($_POST['newUser'])){

    if(!empty($_POST['password'])){

        if ($_FILES['userUpload']['error'] !== UPLOAD_ERR_OK) {
            if($_FILES['userUpload']['error'] === 4){
                addedUser(1,"valid");
                echo("User Updated");
                sleep(2);
                header("Location:  /learning/cms/admin/users.php");
            }else{
            die("Upload failed with error code " . $_FILES['userUpload']['error']);
            }
         }
         
         $info = getimagesize($_FILES['userUpload']['tmp_name']);
         if ($info === FALSE) {
            die("Unable to determine image type of uploaded file");
         }elseif (($info[2] !== IMAGETYPE_GIF) && ($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
            die("Not a gif/jpeg/png");
         }else{
                    $nameImage =  $_FILES['userUpload']['name'];
                    addedUser($nameImage,"valid");
                    move_uploaded_file($_FILES['userUpload']['tmp_name'], "../images/users/$nameImage");
                    echo("User Updated");
                    sleep(2);
                    header("Location:  /learning/cms/admin/users.php");
              
        }
    
    }else if(empty($_POST['password'])){
        echo "<h2>Please Enter Password!</h2>";
    }



}






                            
?>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Add New User
            <!-- <small>Author</small> -->
        </h1>

    </div>


        <form action="" method="post" enctype="multipart/form-data">
   
            <div class="form-group">
                <label for="username"> Username </label>
                <input type="text" name="username" class="form-control" >
            </div>

           

            <div class="form-group">
                <label for="userFirstname">Firstname</label>
                <input type="text" name="userFirstname" class="form-control" >
            </div>
            <div class="form-group">
                <label for="userLastname">Lastname</label>
                <input type="text" name="userLastname" class="form-control">
            </div>
            <div class="form-group">
                <label for="userUpload">Upload</label>
                <input type="file" name="userUpload" >
            </div>
            <div class="form-group">
                <label for="userEmail">Email</label>
                <input type="email" name="userEmail" class="form-control" >
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control" >
            </div>

            <div class="form-group">
                <label for="role">Role</label><br>
                <select name="role">
                    <option value="admin">Admin</option>
                    <option value="subscriber" selected>Subscriber</option>
                </select>
            </div>
            

            <div class="form-group">
                <input type="submit" name="newUser" value="Add User">
            </div>

        </form>




</div>




<!-- /.row -->