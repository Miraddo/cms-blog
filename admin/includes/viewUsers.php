<?php

function actionDelete($key){
    global $stmt;
    $query = $stmt->connect()->prepare("DELETE FROM users WHERE userID = ?");
    $query->bindValue(1,$key,PDO::PARAM_INT);
    $query->execute();
}

function actionActive($key){
    global $stmt;
    $query = $stmt->connect()->prepare("UPDATE users SET userStatus='active'  WHERE userID = ?");
    $query->bindValue(1,$key,PDO::PARAM_INT);
    $query->execute();
}

function actionDeactive($key){
    global $stmt;
    $query = $stmt->connect()->prepare("UPDATE users SET userStatus='deactive'  WHERE userID = ?");
    $query->bindValue(1,$key,PDO::PARAM_INT);
    $query->execute();
}

function statusUser($id,$val){
    global $stmt;
    $cQuery = $stmt->connect()->prepare("UPDATE users SET userStatus=:sts WHERE userID=:id");
    $cQuery->bindValue(":id",$id,PDO::PARAM_INT);
    if ($val === "active"){
        $cQuery->bindValue(":sts","deactive",PDO::PARAM_STR);
    }else if($val === "deactive"){
        $cQuery->bindValue(":sts","active",PDO::PARAM_STR);
    }

    $cQuery->execute();
    
}

if(isset($_GET['uVal'])){
    statusUser($_GET['uID'],$_GET['uVal']);
}


    if (isset($_POST['checkBoxArray'])) {

        switch ($_POST['bulkOption']) {
            case 'active':
                foreach ($_POST['checkBoxArray'] as $array => $value) {
                    actionActive($value);
                }
                break;
            case 'deactive':
                foreach ($_POST['checkBoxArray'] as $array => $value) {
                    actionDeactive($value);
                }
                break;
            case 'delete':
                foreach ($_POST['checkBoxArray'] as $array => $value) {
                    actionDelete($value);
                }
                break;
           
        }


    }



?>



<!-- Page Heading -->


<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            View All Posts
        </h1>

    </div>









    <div class="col-lg-12">

    <form action="" method="post" >

    <div class="col-md-4" style="margin-bottom:20px;">
            <select class="form-control w-25" name="bulkOption">
                <option selected>Select Options</option>
                <option value="active">Active</option>
                <option value="deactive">Deactive</option>
                <option value="delete">Delete</option>
            </select>
        </div>

        <div class="col-md-4" style="margin-bottom:20px;">
            <input type="submit" name="submit" class="btn btn-sm btn-success" value="Apply">
        </div>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                <th><input type="checkbox" name="selectAllBoxes" id="allBoxes"></th>

                    <th>ID</th>
                    <th>Username</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>userImage</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Status</th>
                  

                </tr>
            </thead>
            <tbody>

                            <?php 
                                
                                    $vUserQuery = $stmt->connect()->query("SELECT * FROM users ORDER BY userID DESC");
                                    $vUserData = $vUserQuery->fetchAll(PDO::FETCH_ASSOC);

                                    foreach($vUserData as $row):
                                
                            ?>

                <tr>
                <td><input type="checkbox" class="checkBoxes" name="checkBoxArray[]" value="<?php echo $row['userID'];?>"></td>

                    <td><?php echo $row['userID'];?></td>
                    <td><?php echo $row['username'];?></td>
                    <td><?php echo $row['userFirstname'];?></td>
                    <td><?php echo $row['userLastname'];?></td>
                    <?php if(isset($row['userImage']) AND $row['userImage'] != ''){ ?>
                        <td>
                        <img class="img-responsive" src="../images/users/<?php echo $row['userImage'];?>" width="64px"> 
                        </td>              
                <?php }else{
                    echo "<td class='info text-danger'> Image Not Found </td> ";
                } ?>
                    
                
                    <td><?php echo $row['userEmail'];?></td>
                    <td><?php echo $row['userRole'];?></td>

             
                    <td>
                        <?php
                                            
                            if ($row['userStatus']=== "active"){
                                echo "<a class='text-danger' href='users.php?source=viewUsers&uID={$row['userID']}&uVal={$row['userStatus']}'>Deactive</a>";

                            }else if ($row['userStatus']=== "deactive"){
                                echo "<a class='text-success' href='users.php?source=viewUsers&uID={$row['userID']}&uVal={$row['userStatus']}'>Active</a>";
                            }
                        ?>
                    </td>
                    <td> <a href="users.php?source=editUser&uID=<?php echo $row['userID'];?>">Edit</a> </td>
                    <td> <a onClick="javascript: alert('deleted')" href="users.php?deleteCode=<?php echo $row['userID'];?>">Delete</a> </td>

                </tr>


                <?php endforeach; ?>

            </tbody>
        </table>
        </form>
    </div>
</div>


<?php

if (isset($_GET['deleteCode'])){
    actionDelete($_GET['deleteCode']);

    header("Location: users.php");

}



?>

<!-- /.row -->