<?php 
                                

function updatedUser($valImg,$pVal){
    global $stmt;
    if ($valImg===1){
        $uPostQuery = $stmt->connect()->prepare("SELECT * FROM users WHERE userID=?");
        $uPostQuery->bindValue(1,$_SESSION['userID'],PDO::PARAM_INT);
        $uPostQuery->execute();
        $uPostData = $uPostQuery->fetchAll(PDO::FETCH_ASSOC); 
        foreach($uPostData as $rowImage){
            $valImg=$rowImage['userImage'];
        }
       
    }
    if($pVal === "valid"){
        $query= $stmt->connect()->prepare("UPDATE users SET  
    
            username=:uname,
            userFirstname=:fname,
            userLastname=:lname,  
            userImage=:img,
            userEmail=:uemail,
            userPassword=:upass
            WHERE userID=:ID");
    
    $query->bindValue(":upass",password_hash($_POST['password'],PASSWORD_BCRYPT), PDO::PARAM_STR);

    }else if($pVal === "empty"){
            $query= $stmt->connect()->prepare("UPDATE users SET  
    
            username=:uname,
            userFirstname=:fname,
            userLastname=:lname,  
            userImage=:img,
            userEmail=:uemail
            WHERE userID=:ID");
    }

    $query->bindValue(":uname", $_POST['username'], PDO::PARAM_STR);
    $query->bindValue(":ID", $_SESSION['userID'], PDO::PARAM_INT);
    $query->bindValue(":fname", $_POST['userFirstname'], PDO::PARAM_STR);
    $query->bindValue(":lname", $_POST['userLastname'], PDO::PARAM_STR);
    $query->bindValue(":img", $valImg, PDO::PARAM_STR);
    $query->bindValue(":uemail", $_POST['userEmail'], PDO::PARAM_STR);
    $query->execute();

    $_SESSION['fname']=$_POST['userFirstname'];
    $_SESSION['lname']=$_POST['userLastname'];
}



if(isset($_POST['updateUser'])){

    if(!empty($_POST['password'])){

        if ($_FILES['userUpload']['error'] !== UPLOAD_ERR_OK) {
            if($_FILES['userUpload']['error'] === 4){
                updatedUser(1,"valid");
                echo("User Updated");
                sleep(2);
                header("Location:  ./");
            }else{
            die("Upload failed with error code " . $_FILES['userUpload']['error']);
            }
         }
         
         $info = getimagesize($_FILES['userUpload']['tmp_name']);
         if ($info === FALSE) {
            die("Unable to determine image type of uploaded file");
         }elseif (($info[2] !== IMAGETYPE_GIF) && ($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
            die("Not a gif/jpeg/png");
         }else{
                    $nameImage =  $_FILES['userUpload']['name'];
                    updatedUser($nameImage,"valid");
                    move_uploaded_file($_FILES['userUpload']['tmp_name'], "../images/users/$nameImage");
                    echo("User Updated");
                    sleep(2);
                    header("Location:  ./");
              
        }
    
    }else if(empty($_POST['password'])){
        if ($_FILES['userUpload']['error'] !== UPLOAD_ERR_OK) {
            if($_FILES['userUpload']['error'] === 4){
                updatedUser(1,"empty");
                echo("User Updated");
                sleep(2);
                header("Location:  ./");
            }else{
            die("Upload failed with error code " . $_FILES['userUpload']['error']);
            }
         }
         
         $info = getimagesize($_FILES['userUpload']['tmp_name']);
         if ($info === FALSE) {
            die("Unable to determine image type of uploaded file");
         }elseif (($info[2] !== IMAGETYPE_GIF) && ($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
            die("Not a gif/jpeg/png");
         }else{
                    $nameImage =  $_FILES['userUpload']['name'];
                    updatedUser($nameImage,"empty");
                    move_uploaded_file($_FILES['userUpload']['tmp_name'], "../images/users/$nameImage");
                    echo("User Updated");
                    sleep(2);
                    header("Location:  ./");
              
        }
    }



}






                            
?>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Edit Profile
            <!-- <small>Author</small> -->
        </h1>

    </div>


        <form action="" method="post" enctype="multipart/form-data">
        <?php
            $vUserQuery = $stmt->connect()->prepare("SELECT * FROM users WHERE userID=?");
            $vUserQuery->bindValue(1,$_SESSION['userID'], PDO::PARAM_INT );
            $vUserQuery->execute();
            $vUserData = $vUserQuery->fetchAll(PDO::FETCH_ASSOC); 
            foreach($vUserData as $row):
        ?>
            <div class="form-group">
                <label for="username"> Username </label>
                <input type="text" name="username" class="form-control" value="<?php echo $row['username'];?>">
            </div>

           

            <div class="form-group">
                <label for="userFirstname">Firstname</label>
                <input type="text" name="userFirstname" class="form-control" value="<?php echo $row['userFirstname'];?>">
            </div>
            <div class="form-group">
                <label for="userLastname">Lastname</label>
                <input type="text" name="userLastname" class="form-control" value="<?php echo $row['userLastname'];?>">
            </div>
            <div class="form-group">
                <label for="userUpload">Upload</label>
                <?php if(isset($row['userImage']) AND $row['userImage'] != ''){ ?>
                        
                    <img class="img-responsive" src="../images/users/<?php echo $row['userImage'];?>" width="64px"> 
                    <br>
                <?php }else{
                    echo " Image Not Found ";
                } ?>
                <input type="file" name="userUpload" value="<?php echo $row['userImage'];?>">
            </div>
            <div class="form-group">
                <label for="userEmail">Email</label>
                <input type="email" name="userEmail" class="form-control" value="<?php echo $row['userEmail'];?>">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control" >
            </div>

            
            <?php endforeach; ?>

            <div class="form-group">
                <input type="submit" class="btn btn-sm btn-success" name="updateUser" value="Update Profile">
            </div>

        </form>




</div>




<!-- /.row -->