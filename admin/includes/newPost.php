<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Add New Post
            <!-- <small>Author</small> -->
        </h1>

    </div>

<?php


function insertPost($valImg){
    global $stmt;
    if ($valImg===1){
        $valImg='';
    }
    $query= $stmt->connect()->prepare("INSERT INTO posts   (postCategoryID, 
    postTitle, 
    postAuthor, 
    postDate,
    postImage, 
    postContent,
    postTags,
    postStatus,
    postViewsCount) 
    VALUES 
    (?,?,?,NOW(),?,?,?,?,0)");
$query->bindValue(1, $_POST['postCategoryID'], PDO::PARAM_INT);
$query->bindValue(2, $_POST['postTitle'], PDO::PARAM_STR);
$query->bindValue(3, $_POST['postAuthor'], PDO::PARAM_STR);
$query->bindValue(4, $valImg, PDO::PARAM_STR);
$query->bindValue(5, $_POST['postContent'], PDO::PARAM_STR);
$query->bindValue(6, $_POST['postTags'], PDO::PARAM_STR);
$query->bindValue(7, $_POST['postStatus'], PDO::PARAM_STR);
$query->execute();
}



if(isset($_POST['createPost'])){

    if ($_FILES['postUpload']['error'] !== UPLOAD_ERR_OK) {
        if($_FILES['postUpload']['error'] === 4){
            insertPost(1);
            echo("
                   
                   <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <strong>Post saved!</strong> Your post saved successfully. to see other posts <a href='posts.php'>click here</a>
                    </div> 
                   ");

        }else{
        die("Upload failed with error code " . $_FILES['postUpload']['error']);
        }
     }
     if(!empty($_FILES['postUpload']['tmp_name'])){
        $info = getimagesize($_FILES['postUpload']['tmp_name']);
        if ($info === FALSE) {
           die("Unable to determine image type of uploaded file");
        }elseif (($info[2] !== IMAGETYPE_GIF) && ($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
           die("Not a gif/jpeg/png");
        }else{
                   $nameImage =  $_FILES['postUpload']['name'];
                   insertPost($nameImage);
                   move_uploaded_file($_FILES['postUpload']['tmp_name'], "../images/$nameImage");
                   echo("
                   
                   <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <strong>Post saved!</strong> Your post saved successfully. to see other posts <a href='posts.php'>click here</a>
                        </div>
                   
                   
                   ");
             
       }
     }
     

}




?>


    <form action="" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="postTitle"> Title </label>
            <input type="text" name="postTitle" class="form-control">
        </div>


            <div class="row">
                <div class="form-group col-md-4">
                    <label for="postStatus">Status</label>
                    <select name="postStatus" class="form-control">

                        <option value='published' selected>Publish</option>
                        <option value='drafted'>Draft</option>
                    

                    </select>
                </div>
                    <div class="form-group col-md-4">
                            <label for="postCategoryID">Category</label>
                            
                            <select name="postCategoryID" class="form-control" id="">
                            <?php 
                            $queryCategory = $stmt->connect()->query("SELECT * FROM categories");
                            $dataCategory = $queryCategory->fetchAll(PDO::FETCH_ASSOC);
                        
                            foreach($dataCategory as $rowData){
                                echo "<option value='{$rowData["catID"]}'>{$rowData['catTitle']}</option>";
                            }
                            
                            ?>


                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="postAuthor">Author</label>
                            <input type="text" name="postAuthor" class="form-control" value="<?php echo $_SESSION['fname'].' '.$_SESSION['lname'] ?>" readonly>
                        </div>
       
            </div>
        <div class="form-group">
            <label for="postUpload">Upload Image</label>
            <input type="file" name="postUpload">
        </div>
        
        <div class="form-group">
            <label for="postContent">Post Content</label>
            <textarea name="postContent" class="form-control" id="editor" >
            
            </textarea>
        </div>

        <div class="form-group">
            <label for="postTags">Tags</label>
            <input type="text" name="postTags" class="form-control">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success" name="createPost" value="Save Post">
        </div>
        
    </form>




</div>




<!-- /.row -->