<?php 
                                

function updatedPost($valImg){
    global $stmt;
    if ($valImg===1){
        $uPostQuery = $stmt->connect()->query("SELECT * FROM posts WHERE postID=".$_GET['pID']."");
        $uPostData = $uPostQuery->fetchAll(PDO::FETCH_ASSOC); 
        foreach($uPostData as $rowImage){
            $valImg=$rowImage['postImage'];
        }
       
    }
    $query= $stmt->connect()->prepare("UPDATE posts SET  
    
    postTitle=:title,
    postAuthor=:author,
    postCategoryID=:pcID,  
    postImage=:img,
    postContent=:content,
    postTags=:tags,
    postStatus=:pStatus
    WHERE postID=:ID");

$query->bindValue(":pcID", $_POST['postCategoryID'], PDO::PARAM_INT);
$query->bindValue(":ID", $_GET['pID'], PDO::PARAM_INT);
$query->bindValue(":title", $_POST['postTitle'], PDO::PARAM_STR);
$query->bindValue(":author", $_POST['postAuthor'], PDO::PARAM_STR);
$query->bindValue(":img", $valImg, PDO::PARAM_STR);
$query->bindValue(":content", $_POST['postContent'], PDO::PARAM_STR);
$query->bindValue(":tags", $_POST['postTags'], PDO::PARAM_STR);
$query->bindValue(":pStatus", $_POST['postStatus'], PDO::PARAM_STR);
$query->execute();
}



if(isset($_POST['updatePost'])){

    if ($_FILES['postUpload']['error'] !== UPLOAD_ERR_OK) {
        if($_FILES['postUpload']['error'] === 4){
            updatedPost(1);
            echo("
                   
                    <div class='alert alert-success alert-dismissible' role='alert'>
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                        <strong>Post updated!</strong> Your post updated successfully. to edit other posts <a href='posts.php'>click here</a> or visit <a href='../post.php?pID={$_GET['pID']}'>this post</a>
                    </div> 
            ");
        }else{
        die("Upload failed with error code " . $_FILES['postUpload']['error']);
        }
     }
     
     if(!empty($_FILES['postUpload']['tmp_name'])){
        $info = getimagesize($_FILES['postUpload']['tmp_name']);
        if ($info === FALSE) {
           die("Unable to determine image type of uploaded file");
        }elseif (($info[2] !== IMAGETYPE_GIF) && ($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
           die("Not a gif/jpeg/png");
        }else{
                   $nameImage =  $_FILES['postUpload']['name'];
                   updatedPost($nameImage);
                   move_uploaded_file($_FILES['postUpload']['tmp_name'], "../images/$nameImage");
                   echo("
                      
                       <div class='alert alert-success alert-dismissible' role='alert'>
                           <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                           <strong>Post updated!</strong> Your post updated successfully. to edit other posts <a href='posts.php'>click here</a> or visit <a href='../post.php?pID={$_GET['pID']}'>this post</a>
                       </div> 
               ");
       }
     }

}






                            
?>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Edit Post
            <!-- <small>Author</small> -->
        </h1>

    </div>


        <form action="" method="post" enctype="multipart/form-data">
        <?php
            $vPostQuery = $stmt->connect()->query("SELECT * FROM posts WHERE postID=".$_GET['pID']."");
            $vPostData = $vPostQuery->fetchAll(PDO::FETCH_ASSOC); 
            foreach($vPostData as $row):
        ?>
            <div class="form-group">
                <label for="postTitle"> Title </label>
                <input type="text" name="postTitle" class="form-control" value="<?php echo $row['postTitle'];?>">
            </div>
<div class="row">
            <div class="form-group col-md-4">
                <label for="postCategoryID">Category</label>
            
                <select name="postCategoryID"class="form-control" id="">
                <?php 
                $queryCategory = $stmt->connect()->query("SELECT * FROM categories");
                $dataCategory = $queryCategory->fetchAll(PDO::FETCH_ASSOC);
            
                foreach($dataCategory as $rowData){
                    echo "<option value='{$rowData["catID"]}'>{$rowData['catTitle']}</option>";
                }
                
                ?>


                </select>
            </div>

            <div class="form-group col-md-4">
                <label for="postAuthor">Author</label>
                <input type="text" name="postAuthor" class="form-control" value="<?php echo $row['postAuthor'];?>">
            </div>
            <div class="form-group col-md-4">
                <label for="postStatus">Status</label>

                <select name="postStatus" class="form-control">
                    <?php 
                        if($row['postStatus'] === "published"){
                            echo "
                            <option value='published' selected>Publish</option>
                            <option value='drafted'>Draft</option>
                            ";
                        }else{
                            echo "
                            <option value='published'>Publish</option>
                            <option value='drafted'  selected>Draft</option>
                            ";
                        }
                    
                    ?>

                </select>
            </div>
 </div>           
            <div class="form-group">
                <label for="postUpload">Upload Image</label>
                <?php if(isset($row['postImage']) AND $row['postImage'] != ''){ ?>
                        
                    <img class="img-responsive" src="../images/<?php echo $row['postImage'];?>" width="140px"> 
                    <br>
                <?php }else{
                    echo " Image Not Found ";
                } ?>
                <input type="file" name="postUpload" value="<?php echo $row['postImage'];?>">
            </div>
            
            <div class="form-group">
                <label for="postContent">Post Content</label>
                <textarea name="postContent" class="form-control" id="editor" cols="30" rows="5"><?php echo $row['postContent'];?></textarea>
            </div>
            <?php endforeach; ?>

            <div class="form-group">
                <label for="postTags">Tags</label>
                <input type="text" name="postTags" class="form-control" value="<?php echo $row['postTags'];?>">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" name="updatePost" value="Update Post">
            </div>

        </form>




</div>




<!-- /.row -->