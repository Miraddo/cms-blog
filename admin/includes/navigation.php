 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">CMS Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">

            <?php
                $time = time();
                $timeOutInSeconds=300;
                $timeOut=$time - $timeOutInSeconds;

                $resultUsersOnline= $stmt->connect()->prepare("SELECT * FROM usersStatus WHERE `time` > ?");
                $resultUsersOnline->bindValue(1,$timeOut,PDO::PARAM_INT);
                $resultUsersOnline->execute();
                $ruData = $resultUsersOnline->rowCount();

                ?>

<li><a>Users Online: <?php echo  $ruData; ?></a></li>

                <li><a href="../">Home Site</a></li>
              
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php 
                        if(isset($_SESSION['fname'])){
                            echo $_SESSION['fname'] . ' ' . $_SESSION['lname'];
                        }else{
                            echo "Guest";
                        }
                    ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="./users.php?source=profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                  
                        <li class="divider"></li>
                        <li>
                            <a href="includes/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>