       
                <!-- /.row -->
                <?php 
                
                function rCount($val,$part){
                    global $stmt;
                    
                    switch($val){
                        case 'posts':
                            if($part === "all" ){
                                $plusQuery = "";
                            }else if($part === "published"){
                                $plusQuery = " WHERE postStatus='published'";
                            }else if($part === "drafted"){
                                $plusQuery = " WHERE postStatus='drafted'";
                            }
                            break;
                        case 'users':
                            if($part === "all" ){
                                $plusQuery = "";
                            }else if($part === "active"){
                                $plusQuery = " WHERE userStatus='active'";
                            }else if($part === "deactive"){
                                $plusQuery = " WHERE userStatus='deactive'";
                            }else if($part === "subscriber"){
                                $plusQuery = " WHERE userRole='subscriber'";
                            }
                            break;
                        case 'comments':
                            if($part === "all" ){
                                $plusQuery = "";
                            }else if($part === "approved"){
                                $plusQuery = " WHERE commentStatus='approved'";
                            }else if($part === "unapproved"){
                                $plusQuery = " WHERE commentStatus='unapproved'";
                            }
                            break;
                        case 'categories':
                            if($part === "all" ){
                                $plusQuery = "";
                            }
                            break;   
                    }




                $returnCount = $stmt->connect()->query("SELECT IFNULL(COUNT(*),0) AS valCount FROM {$val} {$plusQuery}");
                    $returnData = $returnCount->fetch();
                    
                    return $returnData['valCount'];
                }
                
                
                ?>



                <div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-file-text fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                  <div class='huge'><?php
                            echo rCount('posts','all');
                  ?></div>
                        <div>Posts</div>
                    </div>
                </div>
            </div>
            <a href="posts.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-comments fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                     <div class='huge'><?php
                            echo rCount('comments','all');
                  ?></div>
                      <div>Comments</div>
                    </div>
                </div>
            </div>
            <a href="comments.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-user fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                    <div class='huge'><?php
                            echo rCount('users','all');
                  ?></div>
                        <div> Users</div>
                    </div>
                </div>
            </div>
            <a href="users.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-list fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class='huge'><?php
                            echo rCount('categories','all');
                  ?></div>
                         <div>Categories</div>
                    </div>
                </div>
            </div>
            <a href="categories.php">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>
                <!-- /.row -->


<div class="row">

    <div class="col-md-12">
        <div id="chart_div" class="chart"></div>

    </div>
</div>



                <script>
                google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);

function drawBasic() {

    var data = google.visualization.arrayToDataTable([
         ['Element', 'Count', { role: 'style' }],
         <?php 
        $publishedPosts = rCount('posts','published');
        $draftedPosts = rCount('posts','drafted');
        $approvedComments = rCount('comments','approved');
        $unapprovedComments = rCount('comments','unapproved');
        $categories = rCount('categories','all');
        $activeUsers = rCount('users','active');
        $deactiveUsers = rCount('users','deactive');
        $subscribers = rCount('users','subscriber');

        echo "['Published Posts', ".intval($publishedPosts)." ,'#337ab7'],";
        echo "['Drafted Posts', ".intval($draftedPosts)." ,'#337ab7'],";
        echo "['Approved Comments',".intval($approvedComments).",'#5cb85c'],";
        echo "['Unapproved Comments',".intval($unapprovedComments).",'#5cb85c'],";
        echo "['Active Users',".intval($activeUsers).",'#f0ad4e'],";
        echo "['Deactive Users',".intval($deactiveUsers).",'#f0ad4e'],";
        echo "['Subscribers',".intval($subscribers).",'#f0ad4e'],";
        echo "['Categories',".intval($categories).",'#d9534f'],";
         ?>// CSS-style declaration
      ]);

      var options = {
        title: '',
        // height: 600,
      };

      var chart = new google.visualization.ColumnChart(
        document.getElementById('chart_div'));

      chart.draw(data, options);
    }
                </script>