<?php 
                                
 

function updateComment(){
    global $stmt;
    $query= $stmt->connect()->prepare("UPDATE comments SET  
    
    commentAuthor=:author,
    commentEmail=:email,  
    commentContent=:content,
    commentStatus=:cStatus
    WHERE commentID=:ID");

$query->bindValue(":email", $_POST['commentEmail'], PDO::PARAM_STR);
$query->bindValue(":ID", $_GET['cID'], PDO::PARAM_INT);
$query->bindValue(":author", $_POST['commentAuthor'], PDO::PARAM_STR);
$query->bindValue(":content", $_POST['commentContent'], PDO::PARAM_STR);
$query->bindValue(":cStatus", $_POST['commentStatus'], PDO::PARAM_STR);
$query->execute();
}



if(isset($_POST['updateComment'])){

    updateComment();

    header("Location: ./comments.php");
}






                            
?>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Edit Comment
            <!-- <small>Author</small> -->
        </h1>

    </div>


        <form action="" method="post" enctype="multipart/form-data">
        <?php
            $vCommentQuery = $stmt->connect()->query("SELECT * FROM comments WHERE commentID=".$_GET['cID']."");
            $vCommentData = $vCommentQuery->fetchAll(PDO::FETCH_ASSOC); 
            foreach($vCommentData as $row):
        ?>

            <div class="form-group">
                <label for="commentAuthor">Author</label>
                <input type="text" name="commentAuthor" class="form-control" value="<?php echo $row['commentAuthor'];?>">
            </div>
            <div class="form-group">
                <!-- <label for="commentStatus">Status</label><br> -->
                <select name="commentStatus" >
                    <?php 
                        if($row['commentStatus']=='approved'){
                            echo "
                                <option value='approved' selected>Approve</option>
                                <option value='unapproced'>Unapprove</option>";
                        }else{
                            echo "
                                <option value='approved'>Approve</option>
                                <option value='unapproced' selected>Unapprove</option>";
                        }
                    ?>
                </select>
                
            </div>
            
            <div class="form-group">
                <label for="commentEmail">Email</label>
                <input type="text" name="commentEmail" class="form-control" value="<?php echo $row['commentEmail'];?>">
            </div>
            <div class="form-group">
                <label for="commentContent">Content</label>
                <textarea name="commentContent" class="form-control" cols="30" rows="5"><?php echo $row['commentContent'];?></textarea>
            </div>
            <?php endforeach; ?>

            <div class="form-group">
                <input type="submit" name="updateComment" value="Update Comment">
            </div>

        </form>




</div>




<!-- /.row -->