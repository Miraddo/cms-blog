<?php 

function actionDelete($key){
    global $stmt;
    $query = $stmt->connect()->prepare("DELETE FROM comments WHERE commentID = ?");
    $query->bindValue(1,$key,PDO::PARAM_INT);
    $query->execute();
}

function actionApprove($key){
    global $stmt;
    $query = $stmt->connect()->prepare("UPDATE comments SET commentStatus='approved'  WHERE commentID = ?");
    $query->bindValue(1,$key,PDO::PARAM_INT);
    $query->execute();
}

function actionUnapprove($key){
    global $stmt;
    $query = $stmt->connect()->prepare("UPDATE comments SET commentStatus='unapproved'  WHERE commentID = ?");
    $query->bindValue(1,$key,PDO::PARAM_INT);
    $query->execute();
}



function approveComment($id,$val){
    global $stmt;
    $cQuery = $stmt->connect()->prepare("UPDATE comments SET commentStatus=:sts WHERE commentID=:id");
    $cQuery->bindValue(":id",$id,PDO::PARAM_INT);
    if ($val === "approved"){
        $cQuery->bindValue(":sts","unapproved",PDO::PARAM_STR);
    }else if($val === "unapproved"){
        $cQuery->bindValue(":sts","approved",PDO::PARAM_STR);
    }

    $cQuery->execute();
    

}

if(isset($_GET['cVal'])){
    approveComment($_GET['cID'],$_GET['cVal']);
}



if (isset($_POST['checkBoxArray'])) {

    switch ($_POST['bulkOption']) {
        case 'approve':
            foreach ($_POST['checkBoxArray'] as $array => $value) {
                actionApprove($value);
            }
            break;
        case 'unapprove':
            foreach ($_POST['checkBoxArray'] as $array => $value) {
                actionUnapprove($value);
            }
            break;
        case 'delete':
            foreach ($_POST['checkBoxArray'] as $array => $value) {
                actionDelete($value);
            }
            break;
       
    }


}

?>


<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            View All Comments
        </h1>

    </div>









    <div class="col-lg-12">
    <form action="" method="post">

    <div class="col-md-4" style="margin-bottom:20px;">
            <select class="form-control w-25" name="bulkOption">
                <option selected>Select Options</option>
                <option value="approve">Approve</option>
                <option value="unapprove">Unapprove</option>
                <option value="delete">Delete</option>
            </select>
        </div>

        <div class="col-md-4" style="margin-bottom:20px;">
            <input type="submit" name="submit" class="btn btn-sm btn-success" value="Apply">
        </div>

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                <th><input type="checkbox" name="selectAllBoxes" id="allBoxes"></th>

                    <th>ID</th>
                    <th>Date</th>
                    <th>Which&nbsp;Post</th>
                    <th>Author</th>
                    <th>Email</th>
                    <th>Content</th>
                    <th>Status</th>

                </tr>
            </thead>
            <tbody>

                <?php 
                                
                                    $vCommentQuery = $stmt->connect()->query("SELECT * FROM comments ORDER BY commentID DESC");
                                    $vCommentData = $vCommentQuery->fetchAll(PDO::FETCH_ASSOC);

                                    foreach($vCommentData as $row):
                                
                            ?>

                <tr>
                <td><input type="checkbox" class="checkBoxes" name="checkBoxArray[]" value="<?php echo $row['commentID'];?>"></td>

                    <td><?php echo $row['commentID'];?></td>
                    <td><?php echo $row['commentDate'];?></td>
                    <td><a href="../post.php?pID=<?php echo $row['commentPostID'];?>"><?php echo $row['commentPostTitle'];?></a></td>
                    <td><?php echo $row['commentAuthor'];?></td>
                    <td><?php echo $row['commentEmail'];?></td>
                    <td><?php echo $row['commentContent'];?></td>
                    <td><?php echo $row['commentStatus'];?></td>

                    <td>
                        <?php
                                            
                            if ($row['commentStatus']=== "approved"){
                                echo "<a class='text-danger' href='comments.php?source=viewComment&cID={$row['commentID']}&cVal={$row['commentStatus']}'>Unapprove</a>";

                            }else if ($row['commentStatus']=== "unapproved"){
                                echo "<a class='text-success' href='comments.php?source=viewComment&cID={$row['commentID']}&cVal={$row['commentStatus']}'>Approve</a>";
                            }
                        ?>
                    </td>
                    <td> <a href="comments.php?source=editComment&cID=<?php echo $row['commentID'];?>">Edit</a> </td>
                  

                    <td> <a onClick="javascript: alert('deleted')" href="comments.php?deleteCode=<?php echo $row['commentID'];?>">Delete</a> </td>

                </tr>



                <?php endforeach; ?>

            </tbody>
        </table>
        </form>
    </div>
</div>


<?php

if (isset($_GET['deleteCode'])){
    actionDelete($_GET['deleteCode']);
    header("Location: comments.php");

}



?>

<!-- /.row -->