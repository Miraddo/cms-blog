<?php include "includes/header.php" ?>

    <div id="wrapper">
 <!-- Navigation -->

<?php include "includes/navigation.php" ?>
       
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <?php include "includes/sidebar.php" ?>

            <?php 
            
                $dis = "hidden";

                saveCat();
                deleteCat();
                returnEditCat();
            


                if(isset($_GET['edit'])){

                    $dis= "show";
                }
            ?>

            <div id="page-wrapper">

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Categories
                <small>Author</small>
            </h1>
        
               
            <div class="col-xs-6">
                <form action="categories.php" method="post">
                    <div class="form-group">
                        <label for="catTitle">Add Category</label>
                        <input class="form-control" type="text" name="catTitle" >
                    </div>
                    <div>
                    <?php 
                        if(isset($_POST['submit'])){
                            if(empty($_POST['catTitle'])){
                                echo "Enter a Value here , this field shouldn't be empty ";
                            
                            }
                    }
                    
                    ?>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" name="submit" value="Add Category">
                    </div>
                </form>


                    <?php updateCat() ?>
                <form action="categories.php" method="post" class="<?php echo $dis; ?>">
          
                    <div class="form-group">
                        <label for="catTitle">Edit Category</label>
                        <input class="form-control" type="text" name="editCat" value="<?php echo $editValue; ?>" >
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" name="editSubmit" value="Update Category">
                    </div>
                    </form>
            </div>





            <div class="col-xs-6">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                           
                        </tr>
                    </thead>
                    <tbody>
                    <?php returnCat(); ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- /.row -->

</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php include "includes/footer.php" ?>


